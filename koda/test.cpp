/**
 * Copyright 2021 Aljaž Farkaš
 **/
#include <iostream>

int main() {
    int *p = new int;

    p[0] = 7;
    std::cout << "My favorite number is: " << p[0] << std::endl;

    delete p;

    return 0;
}
